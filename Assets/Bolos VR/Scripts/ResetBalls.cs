using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetBalls : MonoBehaviour
{

    public GameObject[] balls;
    public bool mode;
    private List<Vector3> ballsTransform = new List<Vector3>();

    // Start is called before the first frame update
    void Start()
    {

        ballsTransform.Add(new Vector3(-15.83f, 0.8f, -18f));

        ballsTransform.Add(new Vector3(-16.33f, 0.8f, -18f));

        ballsTransform.Add(new Vector3(-16.83f, 0.8f, -18f));

        ballsTransform.Add(new Vector3(-17.33f, 0.8f, -18f));


    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ChangeMode()
    {

        if (!mode)
            mode = true;
        else mode = false;
    }


    public void moreBalls()
    {

        for(int i = 0; i<ballsTransform.Count; i++)
        {
            GameObject newBall = Instantiate(balls[i]);
            newBall.GetComponent<ExplodingBalll>().setMode(mode);
            newBall.transform.position = ballsTransform[i];
        }  
    }

}
