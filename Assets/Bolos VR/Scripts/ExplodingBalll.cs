using Autohand.Demo;
using Autohand;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEngine.ParticleSystem;

public class ExplodingBalll : MonoBehaviour
{

    public float explosionForce = 100;
    public float explosionRadius = 10;
    private ParticleSystem mParticles;
    private AudioSource mSound;
    public bool modoFun;

    // Start is called before the first frame update
    void Start()
    {
        mParticles = this.GetComponent<ParticleSystem>();
        mSound = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
    }


    public void ChangeMode()
    {

        if (!modoFun)
         modoFun = true;
        else modoFun = false;
    }

    public void setMode(bool mode)
    {
        modoFun |= mode;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag.Equals("Bowling") && modoFun)
            Explode();
    }

    void Explode()
    {
        ParticleSystem.MainModule pmain = mParticles.main;
        mParticles.lights.light.color = new Color(Random.Range(0F, 1F), Random.Range(0, 1F), Random.Range(0, 1F));
        pmain.startColor = new Color(Random.Range(0F, 1F), Random.Range(0, 1F), Random.Range(0, 1F));
        mParticles.Play();
        mSound.Play();
        var hits = Physics.OverlapSphere(this.transform.position, explosionRadius);
        foreach (var hit in hits)
        {
            if (AutoHandPlayer.Instance.body == hit.attachedRigidbody)
            {
                AutoHandPlayer.Instance.DisableGrounding(0.05f);
                var dist = Vector3.Distance(hit.attachedRigidbody.position, this.transform.position);
                explosionForce *= 2;
                hit.attachedRigidbody.AddExplosionForce(explosionForce - explosionForce * (dist / explosionRadius), this.transform.position, explosionRadius);
                explosionForce /= 2;
            }
            if (hit.attachedRigidbody != null)
            {
                var dist = Vector3.Distance(hit.attachedRigidbody.position, this.transform.position);
                hit.attachedRigidbody.AddExplosionForce(explosionForce - explosionForce * (dist / explosionRadius), this.transform.position, explosionRadius);
            }
        }
        
        StartCoroutine("Die");
       
    }

    IEnumerator Die()
    {
        yield return new WaitForSeconds(0.5f);
        Destroy(this.gameObject);
    }

}
