using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pins : MonoBehaviour
{
    private AudioSource mSound;
    // Start is called before the first frame update
    void Start()
    {
        mSound = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision collision)
    {
       if (collision.gameObject.tag.Equals("Bowling"))
        mSound.Play();
    }

}
