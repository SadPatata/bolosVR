using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Reset : MonoBehaviour
{

    public GameObject pin;

    public GameObject swipper;

    public ArrayList pinsTransform = new ArrayList();

    // Start is called before the first frame update
    void Start()
    {

        swipper.SetActive(false);

        pinsTransform.Add(new Vector3(-22.65f, 0.148f, -16.9f));

        pinsTransform.Add(new Vector3(-22.8f, 0.148f, -16.8f));

        pinsTransform.Add(new Vector3(-22.8f, 0.148f, -17f));

        pinsTransform.Add(new Vector3(-22.95f, 0.148f, -16.9f));

        pinsTransform.Add(new Vector3(-22.95f, 0.148f, -16.7f));

        pinsTransform.Add(new Vector3(-22.95f, 0.148f, -17.1f));

        pinsTransform.Add(new Vector3(-23.1f, 0.148f, -17.2f));

        pinsTransform.Add(new Vector3(-23.1f, 0.148f, -17f));

        pinsTransform.Add(new Vector3(-23.1f, 0.148f, -16.8f));

        pinsTransform.Add(new Vector3(-23.1f, 0.148f, -16.6f));


    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void spawnPins()
    {
        foreach (Vector3 p in pinsTransform) {
            GameObject newPin = Instantiate(pin);
            newPin.transform.position = p;
        }    
    }

    public void swipePins()
    {
        swipper.SetActive(true);
        swipper.gameObject.transform.position = new Vector3(-21f, 0.42f, -16.9f);
        swipper.gameObject.GetComponent<Rigidbody>().velocity = new Vector3(-10, 0, 0);
        StartCoroutine("deactivate");
    }

    IEnumerator deactivate()
    {

        yield return new WaitForSeconds(3f);
        swipper.SetActive(false);
    }

}
