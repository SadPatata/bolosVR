using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModoIker : MonoBehaviour
{

    public GameEvent mEvent;
    public GameObject buttonColor;
    private bool mode = false;

    // Start is called before the first frame update
    void Start()
    {
        buttonColor.GetComponent<MeshRenderer>().material.color = Color.red;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void raise()
    {
        mEvent.Raise();
        if (mode)
        {
            mode = false;
            buttonColor.GetComponent<MeshRenderer>().material.color = Color.red;
        }
        else
        {
            mode = true;
            buttonColor.GetComponent<MeshRenderer>().material.color = Color.green;
        }
        
    }
}
